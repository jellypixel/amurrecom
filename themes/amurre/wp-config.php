<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

/**
 * Database connection information is automatically provided.
 * There is no need to set or change the following database configuration
 * values:
 *   DB_HOST
 *   DB_NAME
 *   DB_USER
 *   DB_PASSWORD
 *   DB_CHARSET
 *   DB_COLLATE
 */

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */

define('AUTH_KEY',         '[.LdzM!@<SjfMKlAe.xQlN5R,lRg0w@j8f$*X)k]K=)-M#whD^Ekl8kc<:YJwpBj');
define('SECURE_AUTH_KEY',  'Ca5dZo^WUv}T5}Vz.C$Zq*Uhr(7WU>CAg]tJzfh_hgWmBSO_Am{@VX<|8-.uN|~(');
define('LOGGED_IN_KEY',    'wMGS>Ph1p]X4K-n1IMu+fmD6{1zP0;K!clbE<--[7WuYbOH0byh=FW$G(k|?<6|6');
define('NONCE_KEY',        '.OO8H:OmYI9>W7h}8k[wGyG*W97y|73y.[,HXLy0g-Gn={Y)rYvs%Nin2$9O44b,');
define('AUTH_SALT',        'S~tTtSYGjS3_q620zbMNm@v7sfQ<E=w@i=2;2[vI7]Eb0jJ8C${yon0QqS7|vY:0');
define('SECURE_AUTH_SALT', 'DOxS*AM|}};=MEYP![I]I9~Bo!BEM5wrf)+[VAlhO0[SnN3K]BDT+eAx92s_EEw@');
define('LOGGED_IN_SALT',   'A(lr^pr|$e<cfY~zz[.vW*F<@J{G*Z-2f1WO*n.?*uN~cw<Cu^LdQv>AKyR,jIJP');
define('NONCE_SALT',       'f+T56g+og7Zt>..xG!%u@m]nBO+pX0RK$3}sx2ARn!=bM$:fL7lP_o{SBtvY-QH1');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
if ( ! defined( 'WP_DEBUG') ) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
