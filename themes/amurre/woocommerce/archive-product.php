<?php
	/**
	* The Template for displaying product archives, including the main shop page which is a post type archive
	*
	* This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
	*
	* HOWEVER, on occasion WooCommerce will need to update template files and you
	* (the theme developer) will need to copy the new files to your theme to
	* maintain compatibility. We try to do this as little as possible, but it does
	* happen. When this occurs the version of the template file will be bumped and
	* the readme will list any important changes.
	*
	* @see https://docs.woocommerce.com/document/template-structure/
	* @package WooCommerce\Templates
	* @version 3.4.0
	*/

	defined( 'ABSPATH' ) || exit;

	get_header( 'shop' );

	/**
	* Hook: woocommerce_before_main_content.
	*
	* @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
	* @hooked woocommerce_breadcrumb - 20
	* @hooked WC_Structured_Data::generate_website_data() - 30
	*/

	// Remove breadcrumb
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20 );
	
	do_action( 'woocommerce_before_main_content' );
?>

	<div class="shop-wrapper">
		<div class="mobile-shop-sidebar">
			<div class="mobile-shop-sidebar-heading">
				<?php echo __( 'Product Category', 'ammurecom' ); ?>
			</div>
			<div class="mobile-shop-cat">
				<?php				
					if ( is_product_category() ) 
					{
						$cats = get_terms( 'product_cat' );		
						$queryObj = get_queried_object();
						$currentCategory = $queryObj->term_id;								
						
						foreach( $cats as $cat ) 
						{
							$catName = $cat->name;
							$catID = $cat->term_id;

							if ( $currentCategory == $catID ) 
							{
								?>									
									<div class="current-cat"><?php echo $catName; ?></div>
									<svg width="32" height="32" viewBox="0 0 24 24" focusable="false"><path d="M12 16.41l-6.71-6.7 1.42-1.42 5.29 5.3 5.29-5.3 1.42 1.42z"></path></svg>
								<?php    
							}
						}  
					}
					else 
					{
						?>									
							<div class="current-cat"><?php echo __( 'All Products', 'ammurecom' ); ?></div>
							<svg width="32" height="32" viewBox="0 0 24 24" focusable="false"><path d="M12 16.41l-6.71-6.7 1.42-1.42 5.29 5.3 5.29-5.3 1.42 1.42z"></path></svg>
						<?php    
					}
				?>
			</div>
		</div>

		<div class="shop-sidebar">
			<div class="shop-sidebar-heading">
				<?php echo __( 'Product Category', 'ammurecom' ); ?>
				<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12 19 6.41z"></path></svg>
			</div>
			<div class="shop-sidebar-content">
				<?php
					// Count All Product
					$cats = get_terms( 'product_cat' );		
					$productCount = 0;
					foreach( $cats as $cat ) 
					{
						$productCount += $cat->count;
					}

					// Checking current category - whether this is archive for all (shop) or category archive
					$queryObj = get_queried_object();
					$currentCategory = $queryObj->term_id;					
				?>

				<?php
					// First entry - all products 
					$allProductsClass = '';
					if ( empty( $currentCategory ) ) {
						$allProductsClass = 'active';
					}
				?>
				<a href="<?php echo get_permalink( wc_get_page_id( 'shop' ) ); ?>" title="<?php echo $catName; ?>" class="<?php echo $allProductsClass; ?>">
					<span><?php echo __( 'All Products', 'ammurecom' ); ?></span>
					<span><?php echo $productCount; ?></span>
				</a>

				<?php
					// Rest of entry - categorized
					foreach( $cats as $cat ) 
					{
						$catName = $cat->name;
						$catLink = get_term_link( $cat );
						$catID = $cat->term_id;
						$catCount = $cat->count;

						$categoryClass = '';
						if ( $currentCategory == $catID ) {
							$categoryClass = 'active';
						}

						?>
							<a href="<?php echo $catLink; ?>" title="<?php echo $catName; ?>" class="<?php echo $categoryClass; ?>">
								<span><?php echo $catName; ?></span>
								<span><?php echo $catCount; ?></span>
							</a>
						<?php                                                                    
					}  
				?>
			</div>
		</div>

		<div class="shop-content-wrapper">
			<div class="shop-heading-wrapper contact-box-enlarge-limit">
				<div class="shop-heading shop-heading-archive-category">
					<?php
						if ( is_product_category() )
						{
							?>
								<div class="shop-heading-left">
									<h1><?php echo $queryObj->name; ?></h1>					
									<div class="product-category-description">
										<?php echo $queryObj->description; ?>
									</div>
								</div>
								<div class="shop-heading-right">
									<button><?php echo __( 'Contact Supplier', 'ammurecom' ); ?></button>
								</div>
							<?php
						}
						else 
						{
							?>
								<h1><?php echo __( 'All Products', 'ammurecom' ); ?></h1>					
								<button><?php echo __( 'Contact Supplier', 'ammurecom' ); ?></button>								
							<?php
						}
					?>
					
				</div>
				<div class="shop-contactform">
					<?php echo do_shortcode( '[contact-form-7 id="181" title="Contact Us"]' ); ?>
				</div>
			</div>

			
			<?php 
				//$shopPage = get_post( wc_get_page_id( 'shop' ) );					
				//echo apply_filters( 'the_content', $shopPage->post_content );					

				// Get all featured products
				global $arrFeaturedProducts;
				$arrFeaturedProducts = array();

				if ( is_product_category() )
				{
					// Featured By Category
					$args = array(
						'post_type' => 'product',
						'meta_query'    => array(
							array(
								'key'       => 'featured_in',
								'value'     => 'category',
								'compare'   => 'LIKE'
							)
						),
						'tax_query' => array(								
							array(
								'taxonomy' => 'product_cat',
								'field' => 'term_id',
								'terms' => $queryObj->term_id 
							)
						),
					);
					$quote_query = new WP_Query($args);

					if ($quote_query->have_posts()) 
					{
						while($quote_query->have_posts()) 
						{
							$quote_query->the_post();								
							global $post;

							array_push( $arrFeaturedProducts, $post->ID );
						}
					}
					wp_reset_postdata();
				}
				else 
				{
					// Featured for all products
					$args = array(
						'post_type' => 'product',
						'meta_query'    => array(
							array(
								'key'       => 'featured_in',
								'value'     => 'allproduct',
								'compare'   => 'LIKE'
							)
						)
					);
					$quote_query = new WP_Query($args);

					if ($quote_query->have_posts()) 
					{
						while($quote_query->have_posts()) 
						{
							$quote_query->the_post();								
							global $post;

							array_push( $arrFeaturedProducts, $post->ID );
						}
					}
					wp_reset_postdata();
				}

				// Show featured products
				if ( !empty( $arrFeaturedProducts ) )
				{					
					?>
						<div class="shop-content">
							<div class="featured-products-block-wrapper horizontal-style">
								<div class="featured-products-block-header">
									<h2><?php _e( 'Featured Products', 'amurrecom' ); ?></h2>
									<?php
										$shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
									?>
									<a href="<?php echo $shop_page_url; ?>" class="showmore"><?php _e( 'Show more', 'amurrecom' ); ?></a>
								</div>
								<div class="featured-products-block-content">
									<?php      
										$args = array(             
											'post_type' => array( 'product' ),
											'post__in' => $arrFeaturedProducts,
											'orderby' => 'post__in'
										);                
										$query = new WP_Query( $args );
										if ( $query->have_posts() ) 
										{
											while ( $query->have_posts() ) 
											{
												$query->the_post();

												global $post;
												$postThumb = get_the_post_thumbnail_url( $post, 'post-thumb' );	

												?>
													<a href="<?php the_permalink(); ?>" class="featured-product-link">
														<div class="featured-product-wrapper">
															<div class="featured-product-image">
																<?php
																	// Thumb
																		if ( !empty( $postThumb ) ) {
																			?>
																				<img src="<?php echo $postThumb; ?>" title="<?php the_title(); ?>">
																			<?php
																		}

																	// Cat
																		global $post;  									                                          
																		$catEl = $catLink = '';
																		$primary_term_id = yoast_get_primary_term_id( 'product_cat', $post );
																		$cat_id = '';

																		if ( $primary_term_id ) {										
																			$catName = get_term( $primary_term_id )->name;
																			$catLink = get_term_link( $primary_term_id );	
																			$cat_id = $primary_term_id;
																			
																			$catEl = '<div class="featured-product-cat">' . $catName . '</div>';
																		}      
																		else 
																		{                     
																			$i = 0;
																			$cats = get_the_terms( $post->ID, 'product_cat' );

																			foreach ($cats as $cat) {
																				$catName = $cat->name;
																				$catLink = get_term_link( $cat );
																				$cat_id = $cat->term_id;

																				if ( $i == 0 )
																				{
																					$catEl = '<div class="featured-product-cat">' . $catName . '</div>';  
																					break;
																				}
																				$i++;           
																			}
																		} 

																		echo $catEl;                                        
																?>                                        
															</div>
															<div class="featured-product-content">
																<div class="featured-product-content-inner">
																	<h3><?php the_title(); ?></h3>

																	<div class="product-list-price-wrapper">
																		<div class="product-list-price">
																			<?php
																				if( have_rows( 'prices', $post->ID  ) )
																				{
																					$arrayPrices = [];                                                        

																					while( have_rows( 'prices', $post->ID  ) ) 
																					{
																						the_row();
																						if ( !empty( get_sub_field( 'cost', $post->ID ) ) )
																						{                                
																							array_push( $arrayPrices, get_sub_field( 'cost', $post->ID  ) );

																							sort( $arrayPrices );                                    
																						}
																					}

																					if ( !empty( $arrayPrices ) )                                                            
																					{
																						$currency = __( '$', 'amurrecom' );

																						if ( count( $arrayPrices ) == 1 ) 
																						{
																							echo $currency . $arrayPrices[0];
																						}
																						else 
																						{
																							echo $currency . $arrayPrices[0] . ' - ' . $currency . end( $arrayPrices );    
																						}
																					}
																				}
																			?>
																		</div>
																	</div>

																	<div class="featured-product-content-excerpt">
																		<?php echo get_the_excerpt(); ?>                                                   
																	</div>                       
																	<span class="showmore"><?php _e( 'Know more', 'amurrecom' ); ?></span>
																</div>
															</div>
														</div>
													</a>
												<?php                       
											}
										} 
										wp_reset_postdata();           										
									?>
								</div>
							</div>
						</div>
					<?php
				}
			?>

			<div class="product-list-wrapper">
				<div class="product-list-heading">
					<h2>
						<?php 
							if ( is_product_category() )
							{
								echo __( 'Other', 'ammurecom' ) . ' ' . $queryObj->name;
							}
							else 
							{
								echo __( 'Other Products', 'ammurecom' );
							}
						?>
					</h2>
				</div>
				<div class="product-list">
					<?php
						// Remove products that's been shown in featured product above
						//global $wp_query;						
						//$query->set('post__not_in', $arrFeaturedProducts);
						//$wp_query->set('post_type', 'post');

						//var_dump($wp_query->query_vars);
											
						if ( wc_get_loop_prop( 'total' ) ) {
						
							while ( have_posts() ) {
								the_post();

								//var_dump( $product->get_id() );

								/**
								* Hook: woocommerce_shop_loop.
								*/
								do_action( 'woocommerce_shop_loop' );

								wc_get_template_part( 'content', 'product' );
							}
						}
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="mobile-contact-wrapper">
		<div class="mobile-contact-button">		
			<?php echo __( 'Contact Supplier', 'ammurecom' ); ?>
		</div>

		<div class="mobile-contact-overlay">
			<div class="mobile-contact-slide">
				<div class="mobile-contact-handle">
				</div>
				<div class="mobile-contact-content">
				</div>
			</div>
		</div>
	</div>

<?php
/*

<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php	
	 // @hooked woocommerce_taxonomy_archive_description - 10
	 // @hooked woocommerce_product_archive_description - 10	 
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php
if ( woocommerce_product_loop() ) {

	 // @hooked woocommerce_output_all_notices - 10
	 // @hooked woocommerce_result_count - 20
	 // @hooked woocommerce_catalog_ordering - 30
	 
	do_action( 'woocommerce_before_shop_loop' );

	woocommerce_product_loop_start();

	if ( wc_get_loop_prop( 'total' ) ) {
		while ( have_posts() ) {
			the_post();

			
			do_action( 'woocommerce_shop_loop' );

			wc_get_template_part( 'content', 'product' );
		}
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 // @hooked woocommerce_pagination - 10
	 
	do_action( 'woocommerce_after_shop_loop' );
} else {
	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 // @hooked wc_no_products_found - 10
	 
	do_action( 'woocommerce_no_products_found' );
}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 
do_action( 'woocommerce_sidebar' );

*/

get_footer( 'shop' );
?>