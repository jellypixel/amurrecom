<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Breadcrumb to product category	
	$catEl = $catLink = '';
	$primary_term_id = yoast_get_primary_term_id( 'product_cat', $product );
	$cat_id = '';
	$backSVG = '<svg width="20" height="20" viewBox="0 0 24 24" focusable="false"><path d="M14.29 18.71L7.59 12l6.7-6.71 1.42 1.42-5.3 5.29 5.3 5.29z"></path></svg>';
	$backPrefix = __( 'Back To Category', 'amurrecom' );

	if ( $primary_term_id ) {										
		$catName = get_term( $primary_term_id )->name;
		$catLink = get_term_link( $primary_term_id );	
		$cat_id = $primary_term_id;
		
		$catEl = '<a href="' . $catLink . '" title="' . $catName . '">' . $backSVG . $backPrefix . ' - ' . $catName . '</a>';
	}      
	else 
	{                                                               
		$cats = get_the_terms( $post->ID, 'product_cat' );

		$i = 0;
		
		foreach( $cats as $cat ) 
		{
			$catName = $cat->name;
			$catLink = get_term_link( $cat );
			$cat_id = $cat->term_id;

			if ( $i == 0 )
			{
				$catEl = '<a href="' . $catLink . '" title="' . $catName . '">' . $backSVG . $backPrefix . ' - ' . $catName . '</a>';  
				break;
			}
			$i++;                                                                    
		}  
	} 
	
	if ( !empty ( $catEl ) )
	{
		?>
			<div class="productcat-breadcrumb">
				<?php echo $catEl; ?>
			</div>
		<?php
	}

/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
	do_action( 'woocommerce_before_single_product' );

	if ( post_password_required() ) {
		echo get_the_password_form(); // WPCS: XSS ok.
		return;
	}
?>

<div id="product-<?php the_ID(); ?>" <?php wc_product_class(); ?>>

	<div class="singleproduct-top">
		<?php
			/**
			* Hook: woocommerce_before_single_product_summary.
			*
			* @hooked woocommerce_show_product_sale_flash - 10
			* @hooked woocommerce_show_product_images - 20
			*/
			do_action( 'woocommerce_before_single_product_summary' );
		?>	

		<div class="singleproduct-top-right">

			<div class="summary entry-summary contact-box-enlarge-limit">
				<div class="summary-content">
					<?php
						/**
						* Hook: woocommerce_single_product_summary.
						*
						* @hooked woocommerce_template_single_title - 5
						* @hooked woocommerce_template_single_rating - 10
						* @hooked woocommerce_template_single_price - 10
						* @hooked woocommerce_template_single_excerpt - 20
						* @hooked woocommerce_template_single_add_to_cart - 30
						* @hooked woocommerce_template_single_meta - 40
						* @hooked woocommerce_template_single_sharing - 50
						* @hooked WC_Structured_Data::generate_product_data() - 60
						*/

						// Remove Meta from this section
						remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

						// Insert Contact Form
						add_action( 'woocommerce_single_product_summary', 'contactform_form', 6 );

						function contactform_form() {
							?>
								<div class="contact-box-contactform-form">
									<?php echo do_shortcode( '[contact-form-7 id="181" title="Contact Us"]' ); ?>
								</div>
							<?php
						}

						// Exec
						do_action( 'woocommerce_single_product_summary' );
					?>

					<div class="share-wrapper">
						<div class="share-button share-el">
							<svg xmlns="http://www.w3.org/2000/svg" height="48" viewBox="0 -960 960 960" width="48"><path d="M120-200v-174q0-79 56-134.5T310-564h416L572-718l42-42 226 226-226 226-42-42 154-154H310q-54 0-92 38t-38 92v174h-60Z"/></svg>
							<div class="share-text"><?php _e( 'Share', 'amurrecom' ); ?></div>					
						</div>

						<div class="share-float-box-wrapper share-el">
							<div class="share-float-box">
								<div class="share-float-content">
									<div class="share-float-heading">
										<?php _e( 'Share Product on:', 'amurrecom' ); ?>
									</div>
									<div class="share-float-row">
										<?php
											$permalink = get_permalink();
											$title = htmlentities( get_the_title() );
											$excerpt = htmlentities( get_the_excerpt() );
											$postThumb = get_the_post_thumbnail_url( $post, 'full' );
											
											$facebookURL = "https://www.facebook.com/sharer/sharer.php?u=" . urlencode($permalink);
											$twitterURL = "http://twitter.com/share?text=" . urlencode($title) . "&amp;url=" . urlencode($permalink);											
											$linkedinURL = "https://www.linkedin.com/shareArticle?mini=true&url=" . urlencode($permalink) . "&title=" . urlencode($title) . "&summary=" . urlencode( $excerpt ) . "&source=LinkedIn";													
											$waURL = 'https://api.whatsapp.com/send?text=' . $permalink;
										?>

										<a href="<?php echo $facebookURL; ?>" title="<?php _e( 'Facebook', 'amurrecom' ); ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/facebook.svg" alt="<?php _e( 'Facebook', 'amurrecom' ); ?>">
											<div class="share-float-name"><?php _e( 'Facebook', 'amurrecom' ); ?></div>
										</a>

										<a href="<?php echo $waURL; ?>" title="<?php _e( 'Whatsapp', 'amurrecom' ); ?>" target="_blank" class="share-wa-link">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/wa.png" alt="<?php _e( 'Whatsapp', 'amurrecom' ); ?>">
											<div class="share-float-name"><?php _e( 'Whatsapp', 'amurrecom' ); ?></div>
										</a>

										<a href="<?php echo $linkedinURL; ?>" title="<?php _e( 'LinkedIn', 'amurrecom' ); ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/linkedin.svg" alt="<?php _e( 'LinkedIn', 'amurrecom' ); ?>">
											<div class="share-float-name"><?php _e( 'LinkedIn', 'amurrecom' ); ?></div>
										</a>

										<a href="<?php echo $twitterURL; ?>" title="<?php _e( 'Twitter', 'amurrecom' ); ?>" target="_blank">
											<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/twitter.svg" alt="<?php _e( 'Twitter', 'amurrecom' ); ?>">
											<div class="share-float-name"><?php _e( 'Twitter', 'amurrecom' ); ?></div>
										</a>
									</div>
								</div>
								<div class="share-float-arrow">
								</div>
								<div class="share-float-arrow-shadow">
								</div>
							</div>
						</div>
					</div>

					<?php
						if ( !empty( get_field( 'prices' ) ) )
						{
							?>
								<div class="pricelist-wrapper">
									<h4><?php _e( 'Price', 'amurrecom' ); ?></h4>
									<?php
										if ( !empty( get_field( 'prices_description' ) ) )
										{
											?>
												<div class="pricelist-description">
													<?php the_field( 'prices_description' ); ?>
												</div>
											<?php
										}
									?>

									<div class="table-responsive">								
										<table>
											<?php
											/*
											<tr>
												<th><?php _e( 'Quantity', 'amurrecom' ); ?></th>
												<th><?php _e( 'Cost', 'amurrecom' ); ?></th>
											</tr>
											*/
											?>

											<?php										
												/*if( have_rows( 'prices' ) )
												{
													while( have_rows( 'prices' ) ) 
													{
														the_row();

														?>
															<tr>
																<td>
																	<?php 				
																		$unit = __( 'unit', 'amurrecom' );
																		$units = __( 'units', 'amurrecom' );															

																		$quantity = number_format( get_sub_field( 'quantity' ), 2 );
																		$quantity = rtrim( $quantity, '0' );
																		$quantity = rtrim( $quantity, '.' );

																		echo sprintf( _n( '%s ' . $unit , '%s ' . $units, $quantity ), $quantity );
																	?>
																</td>
																<td>
																	<?php
																		$currency = __( '$', 'amurrecom' );
																		$singular = __( 'Unit', 'amurrecom' );

																		$cost = number_format( get_sub_field( 'cost' ), 2 );
																		$cost = rtrim( $cost, '0' );
																		$cost = rtrim( $cost, '.' );																
																		
																		echo $currency . $cost . ' / ' . $singular;
																	?>
																</td>
															</tr>
														<?php
													}
												}*/

												// Qty
												if( have_rows( 'prices' ) )
												{
													?>
														<tr>
															<th>Quantity</th>
															<?php
																while( have_rows( 'prices' ) ) 
																{
																	the_row();

																	?>
																		<td>
																			<?php 				
																				/*$unit = __( 'unit', 'amurrecom' );
																				$units = __( 'units', 'amurrecom' );															*/

																				$quantity = number_format( get_sub_field( 'quantity' ), 2 );
																				$quantity = rtrim( $quantity, '0' );
																				$quantity = rtrim( $quantity, '.' );

																				echo $quantity;
																			?>
																		</td>																
																	<?php
																}
															?>
														</tr>
													<?php													
												}

												// Cost
												if( have_rows( 'prices' ) )
												{
													?>
														<tr>
															<th>Cost</th>
															<?php
																while( have_rows( 'prices' ) ) 
																{
																	the_row();

																	?>
																		<td>
																			<?php
																				$currency = __( '$', 'amurrecom' );																		

																				$cost = number_format( get_sub_field( 'cost' ), 2 );
																				$cost = rtrim( $cost, '0' );
																				$cost = rtrim( $cost, '.' );																
																				
																				echo $currency . $cost;
																			?>
																		</td>
																	<?php
																}
															?>
														</tr>
													<?php													
												}
											?>
										</table>
									</div>
								</div>
							<?php
						}
					?>

					

					<div class="shipping-details">
						<h4><?php _e( 'Shipping Details', 'amurrecom' ); ?></h4>
						<?php
							the_field( 'shipping_details');
						?>
					</div>
				</div>
				
				<div class="contact-box-floating">
					<?php
						$alibabaURL = get_field( 'alibaba_url', 'option' );
						$madeinchinaURL = get_field( 'made_in_china_url', 'option' );
						$facebookURL = get_field( 'facebook_url', 'option' );
						$whatsappURL = get_field( 'whatsapp_link', 'option' );
					?>
					<div class="contact-box-wrapper">
						<div class="contact-box-section">
							<div class="contact-box-title">
								<?php _e( 'Order On', 'amurrecom' ); ?>                                    
							</div>
							<div class="contact-box-list">
								<a href="<?php echo $alibabaURL; ?>" target="_blank">
									<div class="contact-box-button contact-box-alibaba">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alibaba.png" alt="<?php _e( 'Alibaba', 'amurrecom' ); ?>     ">
										<?php _e( 'Alibaba', 'amurrecom' ); ?>                                            
									</div>
								</a>
								<a href="<?php echo $madeinchinaURL; ?>" target="_blank">
									<div class="contact-box-button contact-box-madeinchina">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mic.png" alt="<?php _e( 'Made in China', 'amurrecom' ); ?>     ">										                                      
									</div>
								</a>
							</div>
						</div>
				
						<div class="contact-box-section">
							<div class="contact-box-title">                                
								<?php _e( 'Chat with us', 'amurrecom' ); ?>
							</div>
							<div class="contact-box-list">
								<a href="<?php echo $facebookURL; ?>" target="_blank">
									<div class="contact-box-button contact-box-fb">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fbm.png" alt="<?php _e( 'Messenger', 'amurrecom' ); ?>">                                                        
										<?php _e( 'Messenger', 'amurrecom' ); ?>
									</div>
								</a>

								<a href="<?php echo $whatsappURL; ?>" target="_blank">
									<div class="contact-box-button contact-box-wa">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/wa.png" alt="<?php _e( 'WhatsApp', 'amurrecom' ); ?>">                                                        
										<?php _e( 'WhatsApp', 'amurrecom' ); ?>
									</div>
								</a>

								<a href="#" class="contact-box-contactform-link">
									<div class="contact-box-button contact-box-contactform fullwidth">
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/contactform.png" alt="<?php _e( 'Contact Supplier', 'amurrecom' ); ?>">                                                        
										<?php _e( 'Contact Supplier', 'amurrecom' ); ?>     
									</div>
								</a>					

								
							</div>
						</div>            
					</div>				

					<div class="singleproduct-keyfeatures">
						<h4><?php _e( 'Key Features', 'amurrecom' ); ?></h4>						
						<?php 
							//the_field( 'key_features' ); 
							//echo apply_filters( 'the_excerpt', get_field( 'key_features', get_the_ID() ) );
							remove_filter ('acf_the_content', 'wpautop');
							$arrKeyFeatures = get_extended( get_field( 'key_features' ) );
							$main = $arrKeyFeatures['main'];
							$extended= $arrKeyFeatures['extended'];						

							echo $main;

							if ( !empty( $extended ) ) 
							{
								?>
									<div class="moretag-link">
										<?php _e( 'Show More', 'amurrecom' ); ?>
										<svg width="20" height="20" viewBox="0 0 24 24" focusable="false"><path d="M12 16.41l-6.71-6.7 1.42-1.42 5.29 5.3 5.29-5.3 1.42 1.42z"></path></svg>
									</div>
									<div class="moretag-extended">
										<?php echo $extended; ?>
									</div>
								<?php
							}
							add_filter ('acf_the_content', 'wpautop');
						?>

					</div>
				</div>
			</div>			

		</div>

	</div>

	<div class="singleproduct-content">
		<h2><?php _e( 'Product Details', 'amurrecom' ); ?></h2>
		<?php
			// Get Content only			
			$product_details = $product->get_data();			

			echo $product_details['description'];
		?>		
	</div>

	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	// Remove Tabs
	remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10 );

	do_action( 'woocommerce_after_single_product_summary' );

	if ( $related_products ) : ?>

	<section class="related products">

		<?php
		$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'You might also like', 'woocommerce' ) );

		

		if ( $heading ) :
			?>
			<h2><?php echo esc_html( $heading ); ?></h2>
		<?php endif; ?>
		
		<?php woocommerce_product_loop_start(); ?>

			<?php foreach ( $related_products as $related_product ) : ?>

					<?php
					$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					wc_get_template_part( 'content', 'product' );
					?>

			<?php endforeach; ?>

		<?php woocommerce_product_loop_end(); ?>

	</section>
	<?php
endif;

	
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
