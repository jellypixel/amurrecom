<?php
/**
 * Related Products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/related.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     3.9.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $related_products ) 
{
	?>
		<section class="related products">
			<?php
				// Heading
				$heading = apply_filters( 'woocommerce_product_related_products_heading', __( 'You might also like', 'woocommerce' ) );	

				if ( $heading ) 
				{
					$catEl = $catLink = '';
					$primary_term_id = yoast_get_primary_term_id( 'product_cat', $product );
					$cat_id = '';				

					if ( $primary_term_id ) {										
						$catName = get_term( $primary_term_id )->name;
						$catLink = get_term_link( $primary_term_id );	
						$cat_id = $primary_term_id;
						
						$catEl = '<a href="' . $catLink . '" title="' . $catName . '" class="showmore">' . __( 'Show more', 'ammurecom' ) . '</a>';
					}      
					else 
					{                                                               
						$cats = get_the_terms( $post->ID, 'product_cat' );

						$i = 0;
						
						foreach( $cats as $cat ) 
						{
							$catName = $cat->name;
							$catLink = get_term_link( $cat );
							$cat_id = $cat->term_id;

							if ( $i == 0 )
							{
								$catEl = '<a href="' . $catLink . '" title="' . $catName . '" class="showmore">' . __( 'Show more', 'ammurecom' ) . '</a>';  
								break;
							}
							$i++;                                                                    
						}  
					} 
					?>
						<div class="relatedproducts-heading">
							<h2><?php echo esc_html( $heading ); ?></h2>
							<?php
								if ( !empty ( $catEl ) )
								{
									echo $catEl;
								}
							?>						
						</div>
					<?php
				}

				// Related
				/*woocommerce_product_loop_start();

				foreach ( $related_products as $related_product )
				{					
					$post_object = get_post( $related_product->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

					wc_get_template_part( 'content', 'product' );					
				}

				woocommerce_product_loop_end();*/

				?>
					<div class="relatedproducts-content-wrapper">
						<div class="flexslider">
                            <ul class="slides">
								<?php
									foreach ( $related_products as $related_product )
									{					
										$post_object = get_post( $related_product->get_id() );

										/*setup_postdata( $GLOBALS['post'] =& $post_object ); // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited, Squiz.PHP.DisallowMultipleAssignments.Found

										wc_get_template_part( 'content', 'product' );					*/



										$postThumb = get_the_post_thumbnail_url( $post_object, 'post-thumb' );											
										
										?>
                                            <li>                                            
                                                <a href="<?php the_permalink( $post_object ); ?>" class="relatedproducts-link">
                                                    <div class="relatedproducts-wrapper">
                                                        <div class="relatedproducts-image">
                                                            <?php
                                                                if ( !empty( $postThumb ) ) {
                                                                    ?>
                                                                        <img src="<?php echo $postThumb; ?>" title="<?php echo get_the_title( $post_object ); ?>">
                                                                    <?php
                                                                }
                                                            ?>                                        
                                                        </div>
                                                        <div class="relatedproducts-content">
                                                            <div class="relatedproducts-content-inner">
                                                                <h4><?php echo get_the_title( $post_object ); ?></h4>

																<span class="viewdetails">View details</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php       
									}
								?>
							</ul>
						</div>
					</div>
				<?php
			?>
		</section>
	<?php
}

wp_reset_postdata();
