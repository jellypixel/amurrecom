<?php
/**
 * Contact Box Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$contact_list = get_field('contact') ?: '';

$alibabaURL = get_field( 'alibaba_url', 'option' );
$madeinchinaURL = get_field( 'made_in_china_url', 'option' );
$facebookURL = get_field( 'facebook_url', 'option' );
$whatsappURL = get_field( 'whatsapp_link', 'option' );

if ( !empty( $contact_list ) )
{
    ?>
        <div class="contact-box-wrapper <?php echo esc_attr($classes); ?>">
            <?php
                // Alibaba/MadeInChina
                if ( in_array( 'alibaba', array_column( $contact_list, 'value' ) ) ||                     
                     in_array( 'madeinchina', array_column( $contact_list, 'value' ) ) )
                {
                    ?>
                        <div class="contact-box-section">
                            <div class="contact-box-title">
                                <?php _e( 'Order On', 'amurrecom' ); ?>                                    
                            </div>
                            <div class="contact-box-list">
                                <?php
                                    foreach( $contact_list as $key => $contact ) 
                                    {
                                        if ( $contact['value'] == 'alibaba' ) 
                                        {
                                            ?>
                                                <a href="<?php echo $alibabaURL; ?>" target="_blank">
                                                    <div class="contact-box-button contact-box-alibaba">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/alibaba.png" alt="<?php _e( 'Alibaba', 'amurrecom' ); ?>">
                                                        <?php _e( 'Alibaba', 'amurrecom' ); ?>                                            
                                                    </div>
                                                </a>
                                            <?php
                                        }

                                        if ( $contact['value'] == 'madeinchina' ) 
                                        {
                                            ?>
                                                <a href="<?php echo $madeinchinaURL; ?>" target="_blank">
                                                    <div class="contact-box-button contact-box-madeinchina">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/mic.png" alt="<?php _e( 'Made in China', 'amurrecom' ); ?>">                                                        
                                                    </div>
                                                </a>
                                            <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    <?php
                }

                // Remove alibaba/madeinchina from array                
                foreach( $contact_list as $key => $contact ) 
                {
                    if ( $contact['value'] == 'alibaba' || $contact['value'] == 'madeinchina' ) 
                    {
                        
                        unset( $contact_list[ $key ] );                        
                    }
                }

                // fb/wa/contact
                if ( in_array( 'fb', array_column( $contact_list, 'value' ) ) ||
                     in_array( 'wa', array_column( $contact_list, 'value' ) ) ||
                     in_array( 'contact', array_column( $contact_list, 'value' ) ) )
                {
                    ?>
                        <div class="contact-box-section">
                            <div class="contact-box-title">                                
                                <?php _e( 'Chat with us', 'amurrecom' ); ?>
                            </div>
                            <div class="contact-box-list">
                                <?php
                                    foreach( $contact_list as $key => $contact ) 
                                    {
                                        if ( $contact['value'] == 'fb' ) 
                                        {
                                            ?>
                                                <a href="<?php echo $facebookURL; ?>" target="_blank">
                                                    <div class="contact-box-button contact-box-fb">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/fbm.png" alt="<?php _e( 'Messenger', 'amurrecom' ); ?>">                                                        
                                                        <?php _e( 'Messenger', 'amurrecom' ); ?>
                                                    </div>
                                                </a>
                                            <?php
                                        }

                                        if ( $contact['value'] == 'wa' ) 
                                        {
                                            ?>
                                                <a href="<?php echo $whatsappURL; ?>" target="_blank">
                                                    <div class="contact-box-button contact-box-wa">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/wa.png" alt="<?php _e( 'WhatsApp', 'amurrecom' ); ?>">                                                        
                                                        <?php _e( 'WhatsApp', 'amurrecom' ); ?>
                                                    </div>
                                                </a>
                                            <?php
                                        }

                                        if ( $contact['value'] == 'contact' ) 
                                        {
                                            ?>
                                                <a href="#" class="contact-box-contactform-link">
                                                    <div class="contact-box-button contact-box-contactform fullwidth">
                                                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/contactform.png" alt="<?php _e( 'Contact Supplier', 'amurrecom' ); ?>">                                                        
                                                        <?php _e( 'Contact Supplier', 'amurrecom' ); ?>     
                                                    </div>
                                                </a>
                                            <?php
                                        }
                                    }
                                ?>
                            </div>
                        </div>
                    <?php
                }                
            ?>
        </div>
    <?php

    if ( in_array( 'contact', array_column( $contact_list, 'value' ) ) ) 
    {
        ?>
            <div class="contact-box-contactform-form">
                <?php echo do_shortcode( '[contact-form-7 id="181" title="Contact Us"]' ); ?>
            </div>
        <?php        
    }
}
?>