<?php
/**
 * Featured Products Block Template.
 */

$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$products = get_field('products') ?: '';
$style = get_field('style') ?: '';

$styleClass = '';
if ( !empty( $style ) )
{
    $styleClass = 'horizontal-style';
}

?>
<div class="featured-products-block-wrapper <?php echo $styleClass; ?> <?php echo esc_attr($classes); ?>">
    <div class="featured-products-block-header">
        <h2><?php _e( 'Featured Products', 'amurrecom' ); ?></h2>
        <?php
            $shop_page_url = get_permalink( wc_get_page_id( 'shop' ) );
        ?>
        <a href="<?php echo $shop_page_url; ?>" class="showmore"><?php _e( 'Show more', 'amurrecom' ); ?></a>
    </div>
    <div class="featured-products-block-content">
        <?php            
            if ( !empty( $products ) )
            { 
                $args = array(             
                    'post_type' => array( 'product' ),
                    'post__in' => $products,
                    'orderby' => 'post__in'
                );                
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) 
                {
                    while ( $query->have_posts() ) 
                    {
                        $query->the_post();

                        global $post;
						$postThumb = get_the_post_thumbnail_url( $post, 'post-thumb' );	

                        ?>
                            <a href="<?php the_permalink(); ?>" class="featured-product-link">
                                <div class="featured-product-wrapper">
                                    <div class="featured-product-image">
                                        <?php
                                            // Thumb
                                                if ( !empty( $postThumb ) ) {
                                                    ?>
                                                        <img src="<?php echo $postThumb; ?>" title="<?php the_title(); ?>">
                                                    <?php
                                                }

                                            // Cat
                                                global $post;  									                                          
                                                $catEl = $catLink = '';
                                                $primary_term_id = yoast_get_primary_term_id( 'product_cat', $post );
                                                $cat_id = '';

                                                if ( $primary_term_id ) {										
                                                    $catName = get_term( $primary_term_id )->name;
                                                    $catLink = get_term_link( $primary_term_id );	
                                                    $cat_id = $primary_term_id;
                                                    
                                                    $catEl = '<div class="featured-product-cat">' . $catName . '</div>';
                                                }      
                                                else 
                                                {                     
                                                    $i = 0;
                                                    $cats = get_the_terms( $post->ID, 'product_cat' );

                                                    foreach ($cats as $cat) {
                                                        $catName = $cat->name;
                                                        $catLink = get_term_link( $cat );
                                                        $cat_id = $cat->term_id;

                                                        if ( $i == 0 )
                                                        {
                                                            $catEl = '<div class="featured-product-cat">' . $catName . '</div>';  
                                                            break;
                                                        }
                                                        $i++;           
                                                    }
                                                } 

                                                echo $catEl;                                        
                                        ?>                                        
                                    </div>
                                    <div class="featured-product-content">
                                        <div class="featured-product-content-inner">
                                            <h3><?php the_title(); ?></h3>

                                            <div class="product-list-price-wrapper">
                                                <div class="product-list-price">
                                                    <?php
                                                        if( have_rows( 'prices', $post->ID  ) )
                                                        {
                                                            $arrayPrices = [];                                                        

                                                            while( have_rows( 'prices', $post->ID  ) ) 
                                                            {
                                                                the_row();
                                                                if ( !empty( get_sub_field( 'cost', $post->ID ) ) )
                                                                {                                
                                                                    array_push( $arrayPrices, get_sub_field( 'cost', $post->ID  ) );

                                                                    sort( $arrayPrices );                                    
                                                                }
                                                            }

                                                            if ( !empty( $arrayPrices ) )                                                            
                                                            {
                                                                $currency = __( '$', 'amurrecom' );

                                                                if ( count( $arrayPrices ) == 1 ) 
                                                                {
                                                                    echo $currency . $arrayPrices[0];
                                                                }
                                                                else 
                                                                {
                                                                    echo $currency . $arrayPrices[0] . ' - ' . $currency . end( $arrayPrices );    
                                                                }
                                                            }
                                                        }
                                                    ?>
                                                </div>
                                            </div>

                                            <div class="featured-product-content-excerpt">
                                                <?php echo get_the_excerpt(); ?>                                                   
                                            </div>                       
                                            <span class="showmore"><?php _e( 'Know more', 'amurrecom' ); ?></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        <?php                       
                    }
                } 
                wp_reset_postdata();           
            }
        ?>
    </div>
</div>