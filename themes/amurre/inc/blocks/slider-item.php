<?php
/**
 * Slider Item Block Template.
 */
 
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$image = get_field('image') ?: '';
$caption = get_field('caption') ?: '';
?>
<li>
    <div class="slider-item-wrapper <?php echo esc_attr($classes); ?>">    
        <?php
            if ( !empty( $image ) )
            {
                ?>
                    <img src="<?php echo $image; ?>">                    
                <?php

                if ( !empty( $caption ) )
                {
                    ?>
                        <div class="slider-item-caption">   
                            <?php echo $caption; ?>
                        </div>
                    <?php
                }
            }
        ?>
    </div>
</li>