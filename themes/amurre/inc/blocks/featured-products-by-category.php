<?php
/**
 * Featured Products By Category Block Template.
 */

$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$product_category = get_field('product_category') ?: '';
$term = get_term_by( 'id', $product_category, 'product_cat' );
$term_url =  get_category_link( $product_category );

?>
<div class="featured-products-by-category-block-wrapper <?php echo esc_attr($classes); ?>">
    <div class="featured-products-by-category-block-header">
        <h2><?php echo $term->name; ?></h2>
        <a href="<?php echo $term_url; ?>" class="showmore"><?php _e( 'Show more', 'amurrecom' ); ?></a>
    </div>
    <div class="featured-products-by-category-block-content">
        <?php
            if ( !empty( $product_category ) )
            {                
                $args = array(             
                    'post_type' => array( 'product' ),
                    'tax_query' => array(                        
                        array(
                            'taxonomy' => 'product_cat',
                            'field' => 'term_id',
                            'terms' => $product_category
                        )
                    ),
                );                
                $query = new WP_Query( $args );
                if ( $query->have_posts() ) 
                {   
                    ?>
                        <div class="flexslider">
                            <ul class="slides">
                                <?php
                                    while ( $query->have_posts() ) 
                                    {
                                        $query->the_post();

                                        global $post;
                                        $postThumb = get_the_post_thumbnail_url( $post, 'post-thumb' );	

                                        ?>
                                            <li>                                            
                                                <a href="<?php the_permalink(); ?>" class="featured-product-by-category-link">
                                                    <div class="featured-product-by-category-wrapper">
                                                        <div class="featured-product-by-category-image">
                                                            <?php
                                                                if ( !empty( $postThumb ) ) {
                                                                    ?>
                                                                        <img src="<?php echo $postThumb; ?>" title="<?php the_title(); ?>">
                                                                    <?php
                                                                }
                                                            ?>                                        
                                                        </div>
                                                        <div class="featured-product-by-category-content">
                                                            <div class="featured-product-by-category-content-inner">
                                                                <h4><?php the_title(); ?></h4>
                                                                <?php
                                                                    // Use ACF price range
                                                                    if ( !empty( get_field( 'price_range', $post->ID ) ) )
                                                                    {
                                                                        ?>
                                                                            <div class="product-list-price-wrapper">
                                                                                <div class="product-list-price">
                                                                                    <?php the_field( 'price_range', $post->ID ); ?>
                                                                                </div>
                                                                            </div>
                                                                        <?php
                                                                    }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php                       
                                    }
                                ?>
                            </ul>
                        </div>
                    <?php
                } 
                wp_reset_postdata();
            }
        ?>
    </div>
</div>