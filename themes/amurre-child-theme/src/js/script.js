var $j = jQuery.noConflict();

var flexslider = { vars:{} };

$j(function(){	
		$j('.slider-group-block-wrapper.slider-style .flexslider').flexslider({
			animation: "slide",		
			prevText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M14.29 18.71L7.59 12l6.7-6.71 1.42 1.42-5.3 5.29 5.3 5.29z"></path></svg>',
			nextText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M9.71 18.71l-1.42-1.42 5.3-5.29-5.3-5.29 1.42-1.42 6.7 6.71z"></path></svg>',
			controlNav: true,
			directionNav: true
		});
		$j('.slider-group-block-wrapper.carousel-style .flexslider').flexslider({
			animation: "slide",		
			animationLoop: false,
			slideshow: false,
			itemWidth: 198,
			itemMargin: 22,
			prevText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M14.29 18.71L7.59 12l6.7-6.71 1.42 1.42-5.3 5.29 5.3 5.29z"></path></svg>',
			nextText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M9.71 18.71l-1.42-1.42 5.3-5.29-5.3-5.29 1.42-1.42 6.7 6.71z"></path></svg>',
			controlNav: false,
			directionNav: true
		});
		$j('.featured-products-by-category-block-content .flexslider').flexslider({
			animation: "slide",		
			animationLoop: false,
			slideshow: false,
			itemWidth: 202,
			itemMargin: 10,
			prevText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M14.29 18.71L7.59 12l6.7-6.71 1.42 1.42-5.3 5.29 5.3 5.29z"></path></svg>',
			nextText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M9.71 18.71l-1.42-1.42 5.3-5.29-5.3-5.29 1.42-1.42 6.7 6.71z"></path></svg>',
			controlNav: false,
			directionNav: true
		});
		$j( '.accordion-block-wrapper' ).first().children( '.accordion-item-wrapper' ).first().addClass( 'active' ).children( '.accordion-item-content' ).show();
		$j( '.accordion-item-title' ).click( function() {
			var thisEl = $j( this );
			var parentEl = $j( this ).parent( '.accordion-item-wrapper' );

			if ( parentEl.hasClass( 'active' ) ) 
			{
				thisEl.siblings( '.accordion-item-content' ).slideUp( 0 );
			}
			else 
			{
				thisEl.siblings( '.accordion-item-content' ).slideDown( 0 );
			}

			parentEl.toggleClass( 'active' );
		})	
		$j( '.contact-box-contactform-link' ).click( function(e){
			e.preventDefault();

			var thisEl = $j( this );			
			var limitEl = thisEl.closest( '.contact-box-enlarge-limit' );

			limitEl.toggleClass( 'active' );
		})

		$j( '.contactus-close' ).click( function() {
			var thisEl = $j( this );
			var limitEl = thisEl.closest( '.contact-box-enlarge-limit' );
			
			limitEl.removeClass( 'active' );
		})
			$j( '.shop-heading button' ).click( function(e){
				e.preventDefault();

				var thisEl = $j( this );				
				var limitEl = thisEl.closest( '.contact-box-enlarge-limit' );

				limitEl.toggleClass( 'active' );
			})
			$j( '.singleproduct-thumb-nav-slider' ).flexslider({
				animation: "slide",
				controlNav: false,
				animationLoop: false,
				slideshow: false,
				prevText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M14.29 18.71L7.59 12l6.7-6.71 1.42 1.42-5.3 5.29 5.3 5.29z"></path></svg>',
				nextText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M9.71 18.71l-1.42-1.42 5.3-5.29-5.3-5.29 1.42-1.42 6.7 6.71z"></path></svg>',
				itemWidth: 67,
				itemMargin: 11,
				move: 5, 
				asNavFor: '#singleproduct-thumb'
			})
			$j( '.share-button' ).click( function(){
				$j( this ).toggleClass( 'active' );
			});

			$j( document ).on( 'click', function(event) {
				var elements = $j('.share-el');
				var isClickedOutside = true;

				elements.each(function() {
					if ( $j( this ).is( event.target ) || $j( this ).has( event.target ).length ) {
						isClickedOutside = false;
						return false;
					}
				});

				if (isClickedOutside) {
					$j( '.share-button' ).removeClass( 'active' );
				}
			});
			$j( 'body.single-product .contactus-wrapper textarea' ).html( "I'm interested in your product: " + $j( 'body.single-product .product_title' ).html() + ". Can you send more product details?" );
			$j('.relatedproducts-content-wrapper .flexslider').flexslider({
				animation: "slide",		
				animationLoop: false,
				itemWidth: 222,
				itemMargin: 10,
				prevText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M14.29 18.71L7.59 12l6.7-6.71 1.42 1.42-5.3 5.29 5.3 5.29z"></path></svg>',
				nextText: '<svg width="24" height="24" viewBox="0 0 24 24" focusable="false"><path d="M9.71 18.71l-1.42-1.42 5.3-5.29-5.3-5.29 1.42-1.42 6.7 6.71z"></path></svg>',
				controlNav: false,
				directionNav: true
			});	
		$j( '.copyable' ).attr( 'contenteditable', true )

		$j( 'body' ).on( 'click', '.copyable', function() {
			var thisEl = $j( this );

			navigator.clipboard.writeText( thisEl.text() );
		})
	
		$j( '.about-link' ).children( 'a' ).addClass( 'showmore' );
		$j( '.mobile-shop-cat' ).click( function() {
			$j( this ).closest( '.shop-wrapper' ).toggleClass( 'active' );
		})

		$j( '.shop-sidebar-heading svg' ).click( function() {
			$j( this ).closest( '.shop-wrapper' ).removeClass( 'active' );
		})
		$j( '.moretag-link' ).click( function() {
			$j( this ).siblings( '.moretag-extended' ).show();
			$j( this ).hide();
		})
}); 

function getGridSize() {
	return ( window.innerWidth < 576 ) ? 1 :
		   ( window.innerWidth < 992 ) ? 2 : 3;
}

$j( window ).resize( function() {	
		var gridSize = getGridSize();		 
		flexslider.vars.minItems = gridSize;
		flexslider.vars.maxItems = gridSize;	
});

// Fixing phone number issue in contact form
jQuery(window).on('load', function() {
	setTimeout(function() {
	  jQuery('.wpcf7-phonetext-country-code').val('+1'); // Default: America
		
	  // Select the element(s) with the 'selected-flag' class
	  const targetElements = jQuery('.selected-flag');
  
	  // Make function to extract phone country
	  const extractCountryCode = (title) => {
		const matches = /\+(\d+)/.exec(title);
		if (matches && matches.length > 1) {
		  return matches[1];
		}
		return '';
	  };
  
	  // Create a new MutationObserver instance
	  const observer = new MutationObserver((mutationsList) => {
		for (let mutation of mutationsList) {
		  console.log(mutation.type);
		  if (mutation.type === 'attributes' && mutation.attributeName === 'title') {
			// Title attribute of an element with the 'selected-flag' class has changed
			const targetElement = mutation.target;
			const newTitle = extractCountryCode(jQuery(targetElement).attr('title'));
			jQuery('.wpcf7-phonetext-country-code').val('+' + newTitle);
		  }
		}
	  });
  
	  // Start observing each target element for attribute changes
	  targetElements.each((index, element) => {
		observer.observe(element, { attributes: true });
	  });
	}, 2000);

	window.dispatchEvent(new Event('resize'));
}); 
