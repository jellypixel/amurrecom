<?php
/**
 * Slider Group Block Template.
 */

$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$allowed_blocks = array( 'acf/slider-item' );
$carousel = get_field( 'carousel_style' );
$carouselClass = '';
if ( !empty( $carousel ) ) {
    $carouselClass = 'carousel-style';
}
else {
    $carouselClass = 'slider-style';
}

?>
<div class="slider-group-block-wrapper <?php echo $carouselClass; ?> <?php echo esc_attr($classes); ?>">
    <div class="flexslider">
        <ul class="slides">
            <?php echo '<InnerBlocks allowedBlocks="' . esc_attr( wp_json_encode( $allowed_blocks ) ) . '" />'; ?>
        </ul>
    </div>
</div>