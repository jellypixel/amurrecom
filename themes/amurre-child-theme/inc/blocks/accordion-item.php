<?php
/**
 * Accordion Item Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create class attribute allowing for custom "className" and "align" values.
$classes = '';
if( !empty($block['className']) ) {
    $classes .= sprintf( ' %s', $block['className'] );
}
if( !empty($block['align']) ) {
    $classes .= sprintf( ' align%s', $block['align'] );
}

$title = get_field('title') ?: '';
$subtitle = get_field('subtitle') ?: '';
?>
<div class="accordion-item-wrapper <?php echo esc_attr($classes); ?>">    
    <div class="accordion-item-title">
        <svg width="24px" height="24px" viewBox="0 0 48 48"><path d="M14.83 16.42L24 25.59l9.17-9.17L36 19.25l-12 12-12-12z"></path><path d="M0-.75h48v48H0z" fill="none"></path></svg>
        <div class="accordion-item-title-primary">
            <?php echo $title; ?>
        </div>
        <div class="accordion-item-title-secondary">
            <?php echo $subtitle; ?>
        </div>        
    </div>
    <div class="accordion-item-content">
        <?php echo '<InnerBlocks />'; ?>  
    </div>  
</div>