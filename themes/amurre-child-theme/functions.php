<?php
	// Constants
		define( 'THEMEDIR', get_template_directory() . '/' );
		define( 'THEMEURI', get_stylesheet_directory_uri() . '/' );

	// Register Style/Script
		// Frontend
			add_action( 'wp_enqueue_scripts', 'enqueue_list', 20 );
		
			function enqueue_list() {
				// Style											
					wp_enqueue_style( 'flexslider-css',  THEMEURI . ( $css_path = 'src/vendor/flex/css/flexslider.css' ) );					
					wp_enqueue_style( 'font-roboto', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap' );							
					wp_enqueue_style( 'default-css',  THEMEURI . ( $css_path = '/dist/style.min.css' ), array( 'generate-style' ), get_version( $css_path ) );					
					
				// Script			
					wp_enqueue_script( 'jquery' );
					wp_enqueue_script( 'flexslider-js', THEMEURI . 'src/vendor/flex/js/jquery.flexslider.js',  array( 'jquery' ), null, true );				
					wp_enqueue_script( 'default-js', THEMEURI . ( $js_path = '/src/js/script.js' ), array( 'jquery' ), get_version( $js_path ), true );

					wp_add_inline_script('default-js', 'const HC =' . json_encode(array(
						'ajaxurl' => admin_url('admin-ajax.php'),
						'nonce' => wp_create_nonce('ajax-search-nonce'),
					)), 'before');	
			}		
	
		// Backend - UNUSED
			add_action('admin_enqueue_scripts', 'admin_enqueue_list', 22);
			
			function admin_enqueue_list() {		
				wp_enqueue_style( 'font-roboto', 'https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap' );		
				wp_enqueue_style( 'admin-css', THEMEURI . ( $css_path = '/dist/style-admin.min.css' ) );
			}

			/*add_action( 'enqueue_block_editor_assets', 'block_editor_scripts' );

			function block_editor_scripts() {
				wp_enqueue_script( 'block-editor', THEMEURI . 'src/js/editor.js', array( 'wp-blocks', 'wp-dom' ) );
			}*/

		// Replaces cache busting versioning
			function get_version($relative_file_path) {
				return date("ymd-Gis", filemtime( get_stylesheet_directory() . $relative_file_path ));
			}
			
	// Prevent Render Blocking
		add_filter( 'style_loader_tag', 'add_google_font_stylesheet_attributes', 10, 2 );
		
		function add_google_font_stylesheet_attributes( $html, $handle ) {
			if ( 'font-roboto' === $handle ) 
			{
				return str_replace( "rel='stylesheet'", "rel='stylesheet' media='print' onload=\"this.media='all'\"", $html );
			}
			
			return $html;
		}
		
	// GeneratePress - Default Settings
		if ( ! function_exists( 'generate_get_defaults' ) ) {
			/**
			 * Set default options
			 *
			 * @since 0.1
			 */
			 
			function generate_get_defaults() {
				
				return apply_filters(
					'generate_option_defaults',
					array(
						'hide_title' => '',
						'hide_tagline' => true,
						'logo' => '',
						'inline_logo_site_branding' => false,
						'retina_logo' => '',
						'logo_width' => '',
						'top_bar_width' => 'full',
						'top_bar_inner_width' => 'contained',
						'top_bar_alignment' => 'right',
						'container_width' => '1180',
						'container_alignment' => 'text',
						'header_layout_setting' => 'fluid-header',
						'header_inner_width' => 'contained',
						'nav_alignment_setting' => is_rtl() ? 'right' : 'left',
						'header_alignment_setting' => is_rtl() ? 'right' : 'left',
						'nav_layout_setting' => 'fluid-nav',
						'nav_inner_width' => 'contained',
						'nav_position_setting' => 'nav-float-right',
						'nav_drop_point' => '',
						'nav_dropdown_type' => 'hover',
						'nav_dropdown_direction' => is_rtl() ? 'left' : 'right',
						'nav_search' => 'disable',
						'nav_search_modal' => true,
						'content_layout_setting' => 'separate-containers',
						'layout_setting' => 'right-sidebar',
						'blog_layout_setting' => 'right-sidebar',
						'single_layout_setting' => 'right-sidebar',
						'post_content' => 'excerpt',
						'footer_layout_setting' => 'fluid-footer',
						'footer_inner_width' => 'contained',
						'footer_widget_setting' => '3',
						'footer_bar_alignment' => 'right',
						'back_to_top' => '',
						'background_color' => 'var(--base-2)',
						'text_color' => 'var(--contrast)',
						'link_color' => 'var(--accent)',
						'link_color_hover' => 'var(--contrast)',
						'link_color_visited' => '',
						'font_awesome_essentials' => true,
						'icons' => 'svg',
						'combine_css' => true,
						'dynamic_css_cache' => true,
						'structure' => 'flexbox',
						'underline_links' => 'always',
						'font_manager' => array(),
						'typography' => array(),
						'google_font_display' => 'auto',
						'use_dynamic_typography' => true,
						'global_colors' => array(
							array(
								'name' => __( 'Primary', 'generatepress' ),
								'slug' => 'primary',
								'color' => '#185abc',
							),	
							array(
								'name' => __( 'Dark', 'generatepress' ),
								'slug' => 'dark',
								'color' => '#202124',
							),
							array(
								'name' => __( 'Foreground', 'generatepress' ),
								'slug' => 'foreground',
								'color' => '#5f6368',
							),
							array(
								'name' => __( 'Background', 'generatepress' ),
								'slug' => 'background',
								'color' => '#ffffff',
							),	
							array(
								'name' => __( 'Menu', 'generatepress' ),
								'slug' => 'menu',
								'color' => '#3c4043',
							)													
						)
					)
				);
			}
		}

	// Theme Setup
		add_action( 'after_setup_theme', 'initial_setup', 12 );
	
		function initial_setup()
		{	
			// Dequeue Leaks
				if ( !is_admin() ) {
					add_action( 'wp_enqueue_scripts', 'dequeue_leaks');
					
					function dequeue_leaks() {
						wp_deregister_style('common');
					}				
				}
		
			// Theme Support
				if ( function_exists( 'add_theme_support' ) ) {		
					add_theme_support( 'post-thumbnails', array( 'post' ) );
					add_theme_support( 'editor-styles' );
					add_theme_support( 'align-wide' );	
					
					// Non square custom logo
					add_theme_support( 'custom-logo', array(
						 'flex-height' => true,
						 'flex-width' => true
					) );						
			
					// Editor - Font Size
					add_theme_support(
						'editor-font-sizes',
						array(
							array(
								'name'      => __( 'H6', 'generatepress' ),
								'shortName' => __( 'H6', 'generatepress' ),
								'size'      => 12,
								'slug'      => 'H6',
							),
							array(
								'name'      => __( 'H5', 'generatepress' ),
								'shortName' => __( 'H5', 'generatepress' ),
								'size'      => 14,
								'slug'      => 'H5',
							),
							array(
								'name'      => __( 'H4 ', 'generatepress' ),
								'shortName' => __( 'Normal', 'generatepress' ),
								'size'      => 16,
								'slug'      => 'H4',
							),
							array(
								'name'      => __( 'H3 ', 'generatepress' ),
								'shortName' => __( 'H3', 'generatepress' ),
								'size'      => 20,
								'slug'      => 'H3',
							),
							array(
								'name'      => __( 'H2 ', 'generatepress' ),
								'shortName' => __( 'H2', 'generatepress' ),
								'size'      => 24,
								'slug'      => 'H2',
							),
							array(
								'name'      => __( 'H1 ', 'generatepress' ),
								'shortName' => __( 'H1', 'generatepress' ),
								'size'      => 30,
								'slug'      => 'H1',
							)
						)
					);
				}
		
			// Image Size
				if ( function_exists( 'add_image_size' ) ) {
					add_image_size( 'post', 1180 );
					add_image_size( 'post-thumb', 590 );
				}
				
			// Sidebar
				/*register_sidebar( array(
					'name'          => 'Copyright',
					'id'            => 'copyright',
					'before_widget' => '<div class="single-widget">',
					'after_widget'  => '</div>',
					'before_title'  => '<h4>',
					'after_title'   => '</h4>',
				) );*/
				
			// Excerpt
				add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

				function custom_excerpt_length( $length ) {
					return 10;
				}			

				add_filter('woocommerce_short_description', 'custom_woocommerce_short_description', 10, 1);

				function custom_woocommerce_short_description($post_excerpt)
				{
					if ( !is_product() ) 
					{
						$post_excerpt = substr($post_excerpt, 0, 10);
					}
					return $post_excerpt;
				}	

			// Remove GP styling
			//remove_action( 'wp_enqueue_scripts', 'generate_enqueue_dynamic_css', 50 );
		}		

	// ACF Options Page
		if( function_exists('acf_add_options_page') ) {
			acf_add_options_page();
		}

	// ACF Blocks
		add_action('acf/init', 'acf_init_block_types');

		function acf_init_block_types() 
		{	
			if( function_exists('acf_register_block_type') ) 
			{
				// Accordion Group
				acf_register_block_type(array(
					'name'              => 'accordion-group',
					'title'             => 'Accordion',
					'description'       => 'Create accordion.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'list-view',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/accordion-group.php',
				));

				// Accordion Item
				acf_register_block_type(array(
					'name'              => 'accordion-item',
					'title'             => 'Accordion Item',
					'description'       => 'Create accordion item.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'align-center',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/accordion-item.php',
				));

				// Slider Group
				acf_register_block_type(array(
					'name'              => 'slider-group',
					'title'             => 'Slider',
					'description'       => 'Create slider.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'list-view',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/slider-group.php',
				));

				// Accordion Item
				acf_register_block_type(array(
					'name'              => 'slider-item',
					'title'             => 'Slide',
					'description'       => 'Create slide.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'align-center',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/slider-item.php',
				));					

				// Featured Products
				acf_register_block_type(array(
					'name'              => 'featured-products',
					'title'             => 'Featured Products',
					'description'       => 'Highlight certain products.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'megaphone',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/featured-products.php',
				));	

				// Featured Products By Category
				acf_register_block_type(array(
					'name'              => 'featured-products-by-category',
					'title'             => 'Featured Category',
					'description'       => 'Highlight certain categories.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'megaphone',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/featured-products-by-category.php',
				));	

				// Contact Box
				acf_register_block_type(array(
					'name'              => 'contact-box',
					'title'             => 'Contact Box',
					'description'       => 'Show contact buttons.',
					'category'          => 'formatting',
					'mode'              => 'preview',
					'icon'				=> 'email',
					'supports'          => array(
						'align' => true,
						'mode' => false,
						'jsx' => true
					),
					'render_template' => 'inc/blocks/contact-box.php',
				));
			}
		}

	// Woocommerce
		// Enable Gutenberg
			add_filter( 'use_block_editor_for_post_type', 'activate_gutenberg_product', 10, 2 );

			function activate_gutenberg_product( $can_edit, $post_type ) {
			if ( $post_type == 'product' ) {
					$can_edit = true;
				}
				return $can_edit;
			}		

		// Enable taxonomy fields for woocommerce with gutenberg on
			add_filter( 'woocommerce_taxonomy_args_product_cat', 'enable_taxonomy_rest' );
			add_filter( 'woocommerce_taxonomy_args_product_tag', 'enable_taxonomy_rest' );
			
			function enable_taxonomy_rest( $args ) {
				$args['show_in_rest'] = true;
				return $args;
			}			

		// Number of related product
		function woo_related_products_limit() {
				global $product;
				$args['posts_per_page'] = 10000;
				return $args;
		}
		
		add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
		function jk_related_products_args( $args ) {
			$args['posts_per_page'] = 1000; // 4 related products
			$args['columns'] = 1; // arranged in 2 columns
			return $args;
		}
?>