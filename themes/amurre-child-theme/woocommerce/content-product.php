<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}

$show = true;

global $arrFeaturedProducts;
if(!empty($arrFeaturedProducts)) {

	if(in_array($product->get_id(), $arrFeaturedProducts))
		$show = false;
}

if($show) {
	?>
	<li <?php wc_product_class( '', $product ); ?>>
		<?php
		/**
		 * Hook: woocommerce_before_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_open - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item' );

		/**
		 * Hook: woocommerce_before_shop_loop_item_title.
		 *
		 * @hooked woocommerce_show_product_loop_sale_flash - 10
		 * @hooked woocommerce_template_loop_product_thumbnail - 10
		 */
		do_action( 'woocommerce_before_shop_loop_item_title' );

		/**
		 * Hook: woocommerce_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_product_title - 10
		 */
		do_action( 'woocommerce_shop_loop_item_title' );

		/**
		 * Hook: woocommerce_after_shop_loop_item_title.
		 *
		 * @hooked woocommerce_template_loop_rating - 5
		 * @hooked woocommerce_template_loop_price - 10
		 */

		// Remove original woocommerce price and rating 
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 ); 
		remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );

		do_action( 'woocommerce_after_shop_loop_item_title' );

		// Use ACF price range
		if ( !empty( get_field( 'prices' ) ) )
		{
			?>
				<div class="product-list-price-wrapper">
					<div class="product-list-price">
						<?php
							if( have_rows( 'prices', $post->ID  ) )
							{
								$arrayPrices = [];                                                        

								while( have_rows( 'prices', $post->ID  ) ) 
								{
									the_row();
									if ( !empty( get_sub_field( 'cost', $post->ID ) ) )
									{                                
										array_push( $arrayPrices, get_sub_field( 'cost', $post->ID  ) );

										sort( $arrayPrices );                                    
									}
								}

								if ( !empty( $arrayPrices ) )                                                            
								{
									$currency = __( '$', 'amurrecom' );

									if ( count( $arrayPrices ) == 1 ) 
									{
										echo $currency . $arrayPrices[0];
									}
									else 
									{
										echo $currency . $arrayPrices[0] . ' - ' . $currency . end( $arrayPrices );    
									}
								}
							}
						?>
					</div>
				</div>
			<?php
		}

		/**
		 * Hook: woocommerce_after_shop_loop_item.
		 *
		 * @hooked woocommerce_template_loop_product_link_close - 5
		 * @hooked woocommerce_template_loop_add_to_cart - 10
		 */
		// Remove Add To Cart
		remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10 );

		do_action( 'woocommerce_after_shop_loop_item' );
		?>
	</li>
<?php } ?>